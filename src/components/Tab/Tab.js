import * as React from 'react';
import {useState} from 'react';
import browser from 'webextension-polyfill';
import {cancel, refresh} from '../../assets/icon';

const Tab = (props) => {
    const [activeTab, setActiveTab] = useState('js');
    const {children} = props;
    return (
        <>
            <div className="tab-container">
                <div className="tab">
                    <div
                        className={`tablinks ${(activeTab === 'js') ? 'active' : ''}`}
                        onClick={() => setActiveTab('js')}
                    >
                        Javascript
                    </div>
                    <div
                        className={`tablinks ${(activeTab === 'css') ? 'active' : ''}`}
                        onClick={() => setActiveTab('css')}
                    >
                        CSS
                    </div>
                </div>
                <div className="tab-content" style={{display: activeTab === 'js' ? 'block' : 'none'}}>
                    {children[0]}
                </div>
                <div className="tab-content" style={{display: activeTab === 'css' ? 'block' : 'none'}}>
                    {children[1]}
                </div>
            </div>
        </>
    );
}

export default Tab;


