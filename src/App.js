import './App.css';
import CodeMirror from '@uiw/react-codemirror';
import {javascript} from '@codemirror/lang-javascript';
import {css} from '@codemirror/lang-css';
import {oneDarkTheme} from '@codemirror/theme-one-dark';
import Tab from './components/Tab/Tab';
import React from 'react';
import browser from 'webextension-polyfill';
import {glob, window} from './assets/icon';

const defaultObject = {
    js: '/* Ajouter du javascript pour qu\'il agisse \n sur toutes les pages du domaine ou seulement celle ci. */',
    css: '/* Ajouter du css pour qu\'il agisse sur \n toutes les pages du domaine ou seulement celle ci. */'
}

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            url: '',
            hostname: '',
            isDomainScope: true,
            domainStorageObject: defaultObject,
            urlStorageObject: defaultObject,
            storageObjectUpdated: false,
        }
    }

    componentDidMount = async () => {
        // TODO : Should do this in another function
        // browser.storage.local.clear();
        const activeTab = await this.getActiveTab();
        const url = activeTab.url ?? ''
        // skip this on firefox config pages
        if (!url.startsWith('about:') || url !== '') {
            const {hostname} = new URL(url);
            const domainStorageObject = await browser.storage.local.get(hostname);
            const urlStorageObject = await browser.storage.local.get(url);
            this.setState({
                url,
                hostname,
                domainStorageObject: this.isStorageObjectRegistered(domainStorageObject) ? domainStorageObject[hostname] : defaultObject,
                urlStorageObject: this.isStorageObjectRegistered(urlStorageObject) ? urlStorageObject[url] : defaultObject
            });
        }
    }

    componentDidUpdate = async () => {
        const {storageObjectUpdated, hostname} = this.state;
        if (storageObjectUpdated) {
            await this.setStorage(this.getActiveStorageObject());
            this.setState({storageObjectUpdated: false})
        }
    }

    setStorage = async (sto) => {
        const {hostname, isDomainScope, url} = this.state
        const toStore = {
            [isDomainScope ? hostname : url] : sto
        }
        return browser.storage.local.set(toStore);
    }

    handleChange = (value, currentPathName, key) => {
        const tempSTO = this.getActiveStorageObject();
        tempSTO[key] = value;
        this.setState(this.state.isDomainScope ?
            {domainStorageObject: tempSTO, storageObjectUpdated: true} :
            {urlStorageObject: tempSTO, storageObjectUpdated: true}
        );
    }

    getActiveStorageObject = () => this.state.isDomainScope ? this.state.domainStorageObject : this.state.urlStorageObject;

    getActivePath = () => this.state.isDomainScope ? this.state.hostname : this.state.url;

    getActiveTab = async () => {
        let tabs = await browser.tabs.query({
            active: true,
            currentWindow: true
        })
        return tabs[0]
    }

    isStorageObjectRegistered = (obj) => {
        return Object.keys(obj).length !== 0 && obj.constructor === Object;
    }

    render() {
        const {domainStorageObject, urlStorageObject, isDomainScope, hostname, url} = this.state;
        const currentScripts = this.getActiveStorageObject();
        const currentPath = this.getActivePath();
        const JsTab = (
            <CodeMirror
                key="js"
                theme={oneDarkTheme}
                value={currentScripts.js}
                width="700px"
                height="300px"
                extensions={[javascript({jsx: true})]}
                onChange={(value) => {
                    this.handleChange(value, currentPath, 'js')
                }}
            />
        );
        const CssTab = (
            <CodeMirror
                key="css"
                theme={oneDarkTheme}
                value={currentScripts.css}
                width="700px"
                height="300px"
                extensions={[css()]}
                onChange={(value) => {
                    this.handleChange(value, currentPath, 'css')
                }}
            />
        );

        return (
            <div className="App">
                <header className="App-header">
                    <Tab
                        isDomainScope={isDomainScope}
                    >
                        {JsTab}
                        {CssTab}
                    </Tab>
                    <div className="scope-chooser">
                        <div className={isDomainScope ? 'active' : ''}
                             onClick={() => this.setState({isDomainScope: true})}>
                            {glob}
                        </div>
                        <div className={!isDomainScope ? 'active' : ''}
                             onClick={() => this.setState({isDomainScope: false})}>
                            {window}
                        </div>
                    </div>
                </header>
            </div>
        )
    }
}

export default App;
