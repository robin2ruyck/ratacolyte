# Getting Started with RatAcolyte Firefox extension

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

This extension allows to inject custom JavaScript and CSS on a domain or a single page.
## Requirements

### install

To launch this app in a safe firefox browser, you should install [https://github.com/mozilla/web-ext](https://github.com/mozilla/web-ext).

```bash
$ npm install --global web-ext
```

## Available Scripts

In the project directory, you can run:

### `npm start`

Since the extension uses "browser" object from [webextension-polyfill](https://www.npmjs.com/package/webextension-polyfill). \
It won't display anything

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run start:firefox`

You must have [web-ext](https://github.com/mozilla/web-ext) installed to run this command.
Build the app and run it in a temporary firefox window for fast testing.
