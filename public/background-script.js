/*
Log that we received the message.
Then display a notification. The notification contains the URL,
which we read from the message.
*/

const injectUserScripts = async (scripts) => {
    if (scripts.domainScriptsContainer) {
        await executeScripts(scripts.domainScriptsContainer);
    }
    if (scripts.urlScriptsContainer) {
        await executeScripts(scripts.urlScriptsContainer);
    }
}

const executeScripts = async (container) => {
    const jsExec = await browser.tabs.executeScript({
        code: container.js
    });
    const cssExec =await browser.tabs.insertCSS({
        code: container.css
    });
}

/*
Assign `injectUserScripts()` as a listener to messages from the content script.
*/
browser.runtime.onMessage.addListener(injectUserScripts);
