
(async () => {
    const {hostname} = new URL(window.location.href);
    const domainStorageObject = await browser.storage.local.get(hostname);
    const urlStorageObject = await browser.storage.local.get(window.location.href);
    if (isStorageObjectRegistered(domainStorageObject) ||  isStorageObjectRegistered(urlStorageObject)) {
        const domainScriptsContainer = domainStorageObject[hostname];
        const urlScriptsContainer = urlStorageObject[window.location.href];
        if(!domainStorageObject && !urlScriptsContainer) return;
        browser.runtime.sendMessage({domainScriptsContainer, urlScriptsContainer});
    }
})();

const isStorageObjectRegistered = (obj) => {
    return Object.keys(obj).length !== 0 && obj.constructor === Object;
}


// TODO : Injecter le script et le css directement ici ?
// TODO : Faire en sorte que le style et et le js s'exec sur un nouvel onglet ouvert par M3 ou 'ouvrir un nouvel onglet'